from flask import Flask, render_template, request, json, url_for, redirect, session
from flask_mysqldb import MySQL
import perpus
# import simplejson
# import koneksi
app = Flask(__name__)
app.secret_key = "hello"
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'perpus'
mysql = MySQL(app)
listpage = perpus.list()


# @app.route('/', methods=['GET','POST'])
# def auth():
@app.route('/',methods=['GET','POST'])
def auth():
    if request.method=="POST":
        session.permanent==True
        username = request.form['username']
        password = request.form['password']
        cursor = mysql.connection.cursor()
        cursor.execute('SELECT * FROM user WHERE username = %s AND password = %s',(username,password))
        data = cursor.fetchall()
        if len(data)>=0:
            session["nama"] = username
            # return session.get('nama')
            return redirect(url_for('listpage'))
        else:
            return redirect(url_for('/'))
    return render_template('auth.html')

@app.route("/logout")
def logout():
    session.pop("nama", None)
    return redirect(url_for('/'))

if __name__=="__main__":
    app.run(debug=True)