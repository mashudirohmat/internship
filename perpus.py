from flask import Flask, render_template, request, json, url_for, redirect, session
from flask_mysqldb import MySQL
# import simplejson
# import koneksi
app = Flask(__name__)
app.secret_key = "hello"
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'perpus'
mysql = MySQL(app)


# @app.route('/', methods=['GET','POST'])
# def auth():
@app.route('/',methods=['GET','POST'])
def auth():
    if request.method=="POST":
        session.permanent==True
        username = request.form['username']
        password = request.form['password']
        cursor = mysql.connection.cursor()
        cursor.execute('SELECT * FROM user WHERE username = %s AND password = %s',(username,password))
        data = cursor.fetchall()
        if len(data)>0:
            session["nama"] = username
            # return session.get('nama')
            return redirect(url_for('list'))
        else:
            return redirect('/')
    return render_template('auth.html')

@app.route("/logout")
def logout():
    session.pop("nama", None)
    return redirect('/')

@app.route('/list', methods=['GET','POST'])
def list():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM buku")
    data = cur.fetchall()
    dataList = []
    if data is not None:
        for item in data:
            dataTempObj = {
                'id'        : item[0],
                'judul'     : item[1],
                'kategori'  : item[2],
                'pengarang' : item[3],
                'penerbit'  : item[4]
            }
            dataList.append(dataTempObj)
            # ren = json.dumps(dataList)
        # return json.loads(dataList)
        return render_template('list.html',ren=dataList)
    else:
        return 'data kosong'
@app.route('/add', methods=['GET', 'POST'])
def add():
    if request.method == "POST":
        data = request.form
        Judul = data['judul']
        Kategori = data['kategori']
        Pengarang = data['pengarang']
        Penerbit = data['penerbit']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO buku(judul, kategori, pengarang, penerbit) VALUES (%s, %s, %s, %s)", (Judul, Kategori, Pengarang, Penerbit))
        mysql.connection.commit()
        cur.close()
        return redirect(url_for("list"))
    return render_template('add.html')

@app.route('/ubah/<int:kode>', methods=['GET','POST'])
def ubah(kode):
    kode = str(kode)
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM buku WHERE id  = %s',(kode))
    data = cursor.fetchall()
    cursor.close()
    return render_template('update.html',row=data)

@app.route('/update', methods=['GET', 'POST'])
def update():
    if request.method == "POST":
        data = request.form
        Id = data['id']
        Judul = data['judul']
        Kategori = data['kategori']
        Pengarang = data['pengarang']
        Penerbit = data['penerbit']
        cur = mysql.connection.cursor()
        cur.execute("UPDATE buku SET judul = %s, kategori = %s, pengarang = %s, penerbit = %s WHERE id = %s", (Judul, Kategori, Pengarang, Penerbit, Id))
        mysql.connection.commit()
        cur.close()
        return redirect(url_for("list"))
    # return render_template('add.html')
@app.route('/delete/<int:kode>', methods=['GET'])
def delete(kode):
    kode = str(kode)
    cursor = mysql.connection.cursor()
    cursor.execute('DELETE FROM buku WHERE id  = %s',(kode))
    mysql.connection.commit()
    return redirect(url_for('list'))

if __name__=="__main__":
    app.run(debug=True)